testCase: Simple Test case for testing visibility queries
=========================================================

In this simple test case, we generate a (unit) sphere of varying
number of triangles, then perform visibility queries from a
quadrangular "source" slightly above that sphere, to a quadrangular
"receiver" slightly below that sphere. 

We plot the average visibility (without applying any weights,
geomoetric terms, etc) for NxN pixels of the receiver, and write out
 result.

The test case in particular stresses growing model complexity (we
increase the tessellation depth of the sphere in each iteration) as
well as parallel visibility queries (we perform visibility queries for
different rows of pixels (on the receiver) in parallel); as well as
varying size of queries (we randomly chose different number of query
rays for each pixel).
