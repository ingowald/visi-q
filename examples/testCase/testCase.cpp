/* ============================================================================= *\
MIT License

Copyright (c) 2018 Ingo Wald

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
\* ============================================================================= */

#include "examples/common/writePPM.h"
#include "ospcommon/vec.h"
#include "ospcommon/tasking/parallel_for.h"
#include "vq/include/visi-q.h"
#include <vector>
#include <random>
// ospcommon
// #include "ospcommon/tasking/tasking_system_handle.h"

namespace vq {
  using ospcommon::vec2f;
  using ospcommon::vec2i;
  using ospcommon::vec3f;
  using ospcommon::vec3i;
  using tasking::parallel_for;
  using tasking::serial_for;

#if 0
  #define MIN_RAYS_PER_PIXEL 4
  #define MAX_RAYS_PER_PIXEL 64
#else  
  #define MIN_RAYS_PER_PIXEL 128
  #define MAX_RAYS_PER_PIXEL 1024
#endif
  struct TriangleMesh
  {
    std::vector<vec3f> vertex;
    std::vector<vec3i> index;
  };

  struct TessellatedSphere : public TriangleMesh {
    TessellatedSphere(int recursions)
    { 
      addSegment(vec3f(-1,0,0),vec3f(0,-1,0),vec3f(0,0,-1),recursions);
      addSegment(vec3f(-1,0,0),vec3f(0,-1,0),vec3f(0,0,+1),recursions);
      addSegment(vec3f(-1,0,0),vec3f(0,+1,0),vec3f(0,0,-1),recursions);
      addSegment(vec3f(-1,0,0),vec3f(0,+1,0),vec3f(0,0,+1),recursions);
      addSegment(vec3f(+1,0,0),vec3f(0,-1,0),vec3f(0,0,-1),recursions);
      addSegment(vec3f(+1,0,0),vec3f(0,-1,0),vec3f(0,0,+1),recursions);
      addSegment(vec3f(+1,0,0),vec3f(0,+1,0),vec3f(0,0,-1),recursions);
      addSegment(vec3f(+1,0,0),vec3f(0,+1,0),vec3f(0,0,+1),recursions);

      std::cout << "Tessellated Sphere: Created " << index.size() << " triangles" << std::endl;
    }

  private:
    void addSegment(const vec3f &a, const vec3f &b, const vec3f &c, int depth)
    {
      if (depth <= 0) {
        index.push_back(vec3i(vertex.size())+vec3i(0,1,2));
        vertex.push_back(a);
        vertex.push_back(b);
        vertex.push_back(c);
      } else {
        const vec3f ab = normalize(a+b);
        const vec3f bc = normalize(b+c);
        const vec3f ca = normalize(c+a);
        addSegment(a,ab,ca,depth-1);
        addSegment(b,bc,ab,depth-1);
        addSegment(c,ca,bc,depth-1);
        addSegment(ab,bc,ca,depth-1);
      }
    }
  };
  
  /* The source that is slightly above the receiver */
  struct Source {
    Source()
      : corner(-2,-2,1.1),edgeX(4,0,0),edgeY(0,4,0)
    {}

    vec3f sample(const vec2f &pos) const { return corner + pos.x*edgeX + pos.y*edgeY; }
  private:
    vec3f corner;
    vec3f edgeX, edgeY;
  };

  /*! represents a single 'pixel' on a numPixels sized receiver plane
      right below the sphere */
  struct ReceiverPixel
  {
    ReceiverPixel(const vec2i &pixelID, const vec2i &numPixels)
      : corner(-2.f+4.f*pixelID.x/float(numPixels.x),
               -2.f+4.f*pixelID.y/float(numPixels.y),
               -1.1),
        edgeX(4.f/numPixels.x,0,0),
        edgeY(0,4.f/numPixels.y,0)
    {
    }

    vec3f sample(const vec2f &pos) const { return corner + pos.x*edgeX + pos.y*edgeY; }
  private:
    vec3f corner;
    vec3f edgeX, edgeY;
  };

  /*! a receiver of NxN pixels, each of which being a float that will,
      eventually, contain the avergae visibility of the source from
      that receiver pixel */
  struct Receiver {
    Receiver(const vec2i &numPixels)
      : numPixels(numPixels)
    {
      pixelValue.resize(numPixels.x*(size_t)numPixels.y);
    }

    void saveAsImage(const std::string &fileName)
    {
      writePPM_grayScale(fileName.c_str(),&pixelValue[0],numPixels.x,numPixels.y);
    }

    void setPixel(const vec2i &pixelID, float value) 
    { 
      pixelValue[pixelID.x+numPixels.x*pixelID.y] = value; 
    }

    std::vector<float> pixelValue;
    vec2i numPixels;
  };

  VQModel makeVqModel(const TriangleMesh &geometry)
  {
    VQModel model = vqNewModel();
    vqAddTriangleMesh(model,
                      &geometry.vertex[0].x,
                      geometry.vertex.size(),
                      sizeof(geometry.vertex[0]),
                      &geometry.index[0].x,
                      geometry.index.size(),
                      sizeof(geometry.index[0]),
                      true/*!< yes, we guarantee this to be persistent */
                      );
    vqBuildModel(model);
    return model;
  }

  void computeIt(const Source &source, 
                 Receiver &receiver,
                 const VQModel model)
  {
    std::random_device rd;
    std::mutex mutex;

    // parallelize this (via TBB) by operating on independent pixels;
    // each pixel computes its own visibility queries
    parallel_for(receiver.numPixels.y,[&](int y){
        uint32_t seed;
        { std::lock_guard<std::mutex> lock(mutex); seed = rd(); }
        std::mt19937 mt(seed);
        std::uniform_int_distribution<int>
          numRaysRandomizer(MIN_RAYS_PER_PIXEL,MAX_RAYS_PER_PIXEL);
        std::uniform_real_distribution<float> areaSampler(0.f,1.f);

        serial_for(receiver.numPixels.x,[&](int x){
            // the pixel on the receiver we're computing in this iteration
            ReceiverPixel pixel(vec2i(x,y),receiver.numPixels);
            size_t numSamplesForThisPixel = numRaysRandomizer(mt);
            std::vector<vec3f> sourceSamples(numSamplesForThisPixel);
            std::vector<vec3f> pixelSamples(numSamplesForThisPixel);
            for (auto &sample : sourceSamples)
              sample = source.sample(vec2f(areaSampler(mt),areaSampler(mt)));
            for (auto &sample : pixelSamples)
              sample = pixel.sample(vec2f(areaSampler(mt),areaSampler(mt)));
            std::vector<int>   resultSamples(numSamplesForThisPixel);
            vqTestVisibilityBetweenPoints(model,
                                          &sourceSamples[0].x,
                                          &pixelSamples[0].x,
                                          &resultSamples[0],
                                          sizeof(sourceSamples[0]),
                                          sourceSamples.size());
                                          
            size_t numVisible = 0;
            for (auto result : resultSamples)
              numVisible += (result != 0);
            receiver.setPixel(vec2i(x,y),numVisible/float(resultSamples.size()));
          });
      });
  }

  extern "C" int main(int ac, const char **av)
  {
    tbb::task_scheduler_init tbb_init;

    vqInit(&ac,&av,NULL);

    // 'size' of the sphere geometry
    size_t numRecursions = 3;
    // size_t numRecursions = 8;
    TessellatedSphere testGeometry(numRecursions);
    VQModel model = makeVqModel(testGeometry);

    Source source;
    Receiver receiver(vec2i(1024));

    computeIt(source,receiver,model);
    vqDestroyModel(model);
    receiver.saveAsImage("testCase.ppm");
    vqTerminate();
    return 0;
  }

}
