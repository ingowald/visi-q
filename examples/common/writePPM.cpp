/* ============================================================================= *\
   MIT License

   Copyright (c) 2018 Ingo Wald

   Permission is hereby granted, free of charge, to any person obtaining a copy
   of this software and associated documentation files (the "Software"), to deal
   in the Software without restriction, including without limitation the rights
   to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
   copies of the Software, and to permit persons to whom the Software is
   furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in all
   copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
   SOFTWARE.
   \* ============================================================================= */

#include "writePPM.h"
#include "ospcommon/memory/malloc.h"
#include <vector>

namespace vq {

  int colorChannelFloatToInt(float f)
  {
    int i = int(256.f*f);
    return std::max(0,std::min(i,255));
  }

  /*! taken from ospray/components/ospcommon/utility/SaveImage.h */
  template <typename COMP_T, int N_COMP, typename PIXEL_T, bool FLIP>
  inline void writeImage(const std::string &fileName,
                         const char *const header,
                         const int sizeX, const int sizeY,
                         const PIXEL_T *const pixel)
  {
    FILE *file = fopen(fileName.c_str(), "wb");
    if (file == nullptr)
      throw std::runtime_error("Can't open file for writeP[FP]M!");

    fprintf(file, header, sizeX, sizeY);
    auto out = STACK_BUFFER(COMP_T, N_COMP*sizeX);
    for (int y = 0; y < sizeY; y++) {
      auto *in = (const COMP_T*)&pixel[(FLIP?sizeY-1-y:y)*sizeX];
      for (int x = 0; x < sizeX; x++)
        for (int c = 0; c < N_COMP; c++)
          out[N_COMP*x + c] = in[4*x + (N_COMP==1?3:c)];
      fwrite(out, N_COMP*sizeX, sizeof(COMP_T), file);
    }
    fprintf(file, "\n");
    fclose(file);
  }

  /*! taken from ospray/components/ospcommon/utility/SaveImage.h */
  inline void writePPM(const std::string &fileName,
                       const int sizeX, const int sizeY,
                       const uint32_t *pixel)
  {
    writeImage<unsigned char, 3, uint32_t, true>(fileName, "P6\n%i %i\n255\n",
                                                 sizeX, sizeY, pixel);
  }

  void writePPM_grayScale(const char *fileName,
                          float *value, size_t sizeX, size_t sizeY)
  {
    std::vector<uint32_t> rgb;
    for (size_t i=0;i<sizeX*sizeY;i++) {
      int c255 = colorChannelFloatToInt(value[i]);
      rgb.push_back((c255 << 0) |
                    (c255 << 8) |
                    (c255 << 16));
    }
    writePPM(fileName,sizeX,sizeY,&rgb[0]);
  }

}
