ADD_SUBDIRECTORY(common)

# a simple test case, of a sphere (in various sizes) in-between two
# quads
ADD_SUBDIRECTORY(testCase)
