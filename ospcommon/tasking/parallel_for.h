/* ============================================================================= *\
MIT License

Copyright (c) 2018 Ingo Wald

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
\* ============================================================================= */

/* iw, jun 2018: code pieces copied over (and subsequently modified
   from Intel's OSPRay/ospcommon, under the following license: */

// ======================================================================== //
// Copyright 2009-2018 Intel Corporation                                    //
//                                                                          //
// Licensed under the Apache License, Version 2.0 (the "License");          //
// you may not use this file except in compliance with the License.         //
// You may obtain a copy of the License at                                  //
//                                                                          //
//     http://www.apache.org/licenses/LICENSE-2.0                           //
//                                                                          //
// Unless required by applicable law or agreed to in writing, software      //
// distributed under the License is distributed on an "AS IS" BASIS,        //
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. //
// See the License for the specific language governing permissions and      //
// limitations under the License.                                           //
// ======================================================================== //

#pragma once

#include "../common.h"
#include <tbb/parallel_for.h>
#include <tbb/task_arena.h>
#include <tbb/task_scheduler_init.h>

namespace vq {
  namespace tasking {

    // NOTE(jda) - This abstraction wraps "fork-join" parallelism, with an
    //             implied synchronizsation after all of the tasks have run.
    template<typename INDEX_T, typename TASK_T>
    inline void parallel_for(INDEX_T nTasks, TASK_T&& fcn)
    {
      // static_assert(is_valid_index<INDEX_T>::value,
      //               "ospcommon::tasking::parallel_for() requires the type"
      //               " INDEX_T to be unsigned char, short, int, uint, long,"
      //               " or size_t.");

      // static_assert(has_operator_method_matching_param<TASK_T, INDEX_T>::value,
      //               "ospcommon::tasking::parallel_for() requires the "
      //               "implementation of method "
      //               "'void TASK_T::operator(P taskIndex), where P is of "
      //               "type INDEX_T [first parameter of parallel_for()].");
      
      tbb::parallel_for(INDEX_T(0), nTasks, std::forward<TASK_T>(fcn));      
    }

    // NOTE(jda) - Allow serial version of parallel_for() without the need to
    //             change the entire tasking system backend
    template<typename INDEX_T, typename TASK_T>
    inline void serial_for(INDEX_T nTasks, const TASK_T& fcn)
    {
      // static_assert(is_valid_index<INDEX_T>::value,
      //               "ospcommon::tasking::serial_for() requires the type"
      //               " INDEX_T to be unsigned char, short, int, uint, long,"
      //               " or size_t.");

      // static_assert(has_operator_method_matching_param<TASK_T, INDEX_T>::value,
      //               "ospcommon::tasking::serial_for() requires the "
      //               "implementation of method "
      //               "'void TASK_T::operator(P taskIndex), where P is of "
      //               "type INDEX_T [first parameter of serial_for()].");

      for (INDEX_T taskIndex = 0; taskIndex < nTasks; ++taskIndex)
        fcn(taskIndex);
    }



    /* NOTE(iw) - This abstraction extends the 'parallel_for' to mixed
       parallel/serial: we logically view the domain of N input tasks
       as grouped into roundUp(N/M) blocks of (at most) M items each;
       then 'itearte over the N/M blocks in parallel, and process each
       block serailly */
    template<int BLOCK_SIZE, typename INDEX_T, typename TASK_T>
    inline void parallel_in_blocks_of(INDEX_T nTasks, TASK_T&& fcn)
    {
      // using namespace traits;
      // static_assert(is_valid_index<INDEX_T>::value,
      //               "ospcommon::tasking::parallel_for() requires the type"
      //               " INDEX_T to be unsigned char, short, int, uint, long,"
      //               " or size_t.");

      // // iw - TODO: fix this
      // static_assert(has_operator_method_matching_param<TASK_T, INDEX_T>::value,
      //               "ospcommon::tasking::parallel_for() requires the "
      //               "implementation of method "
      //               "'void TASK_T::operator(P taskIndex), where P is of "
      //               "type INDEX_T [first parameter of parallel_for()].");

      INDEX_T numBlocks = (nTasks+BLOCK_SIZE-1)/BLOCK_SIZE;
      parallel_for(numBlocks,[&](INDEX_T blockID){
          INDEX_T begin = blockID * (INDEX_T)BLOCK_SIZE;
          INDEX_T end   = std::min(begin+(INDEX_T)BLOCK_SIZE,nTasks);
          fcn(begin,end);
        });
    }

  } // ::vq::tasking
} //::vq
