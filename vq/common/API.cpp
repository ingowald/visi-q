/* ============================================================================= *\
MIT License

Copyright (c) 2018 Ingo Wald

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
\* ============================================================================= */

// public API
#include "vq/include/visi-q.h"
// our stuff
#include "vq/common/Device.h"

#undef VQ_API
#define VQ_API extern "C" 

namespace vq {

  std::shared_ptr<Device> g_device;

  /*! our 'hook' into the visiq-embree library - all we have to see is
      that we can create a device, so we don't actually have to
      include any header files from that backend at all */
  namespace embree {
    std::shared_ptr<Device> createEmbreeDevice(int *ac, 
                                               const char ***av, 
                                               const char *jsonConfig);
  }
  namespace opencl {
    std::shared_ptr<Device> createOpenCLDevice(int *ac, 
                                               const char ***av, 
                                               const char *jsonConfig);
  }

  /*! initially visi-q library; this should be called exactly once,
    before any other call to this library, jsonConfig is a optional
    configuration string that specifies - in json - some configuration
    parametrs (using NULL means 'ignore') */
  VQ_API void vqInit(int *ac, const char ***av, const char *jsonConfig)
  {
#if VQ_HAVE_OPENCL
    if (getenv("VQ_DEVICE") && (std::string(getenv("VQ_DEVICE"))=="opencl"))
      g_device = vq::opencl::createOpenCLDevice(ac,av,jsonConfig);
    else
#endif
      g_device = vq::embree::createEmbreeDevice(ac,av,jsonConfig);
  }
  
  /*! destroy a given model, and release all its allocated memory */
  VQ_API void    vqDestroyModel(VQModel model)
  {
    assert(g_device);
    return g_device->destroyModel((Device::ModelHandle)model);
  }

  /*! tell visi-q library to terminate, and clean up all remaining
    state. this should be called exactly once at the end, and
    (obviously) no calls to this library should be done after that */
  VQ_API void    vqTerminate()
  {
    g_device = nullptr;
  }


/*! create a new model into which we can add geometries, and into
    which new visibility queries can be issues (after vqBuildModel has been called) */
  VQ_API VQModel vqNewModel()
  {
    assert(g_device);
    return g_device->newModel();
  }

/*! add a new triangl emesh to this model, with the given vertex and
    index arrays. This function can be called multiple times, but only
    after a model has been created (\see vqNewModel), and before this
    model gets built (\see vqBuildModel) */
  VQ_API void    vqAddTriangleMesh(VQModel model,
                                   const float   *vertexArray, 
                                   size_t         numVertices, 
                                   size_t         sizeOfVertex,
                                   const int32_t *triangleArray, 
                                   size_t         numTriangles, 
                                   size_t         sizeOfTriangle,
                                   /*! if set to true, the back-end can
                                     rely on the data to be
                                     persistent; ie, to not be moved,
                                     deallocated, or modified, and
                                     will thus share the data arrays
                                     rather than replicating them */
                                   bool           sharedPersistentData)
  {
    assert(g_device);
    g_device->addTriangleMesh((Device::ModelHandle)model,
                              vertexArray,numVertices,sizeOfVertex,
                              triangleArray,numTriangles,sizeOfTriangle,
                              sharedPersistentData);
  }
  
  /*! build a model; meaning that its acceleration structure can and
    should now be built. This function should be called exactly once
    for each model, after all its geometries have been added, and
    before any visibility queries may be made */
  VQ_API void    vqBuildModel(VQModel model)
  {
    assert(g_device);
    g_device->buildModel((Device::ModelHandle)model);
  }

  /*! perform a sequence of visbility test between an array of points A
    and an array of other points B, such that we always test the
    visibility between the i'th point in A and its corresponding i'th
    point in B. Result will be written into isVisible[] array, with 0
    meaninng the two points are not mutually visible, and anything
    else meaning that yes, they are mutually visible. Note this
    function is thread-safe, meaning different threads may perform
    visisbliity queries in parallel */
  VQ_API void    vqTestVisibilityBetweenPoints(VQModel      model,
                                               const float *pointsA,
                                               const float *pointsB,
                                               int32_t     *isVisible,
                                               size_t       sizeOfPoint,
                                               size_t       numPoints)
  {
    assert(g_device);
    g_device->testVisibilityBetweenPoints((Device::ModelHandle)model,
                                          pointsA,pointsB,isVisible,sizeOfPoint,numPoints);
  }

}
