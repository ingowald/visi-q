/* ============================================================================= *\
MIT License

Copyright (c) 2018 Ingo Wald

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
\* ============================================================================= */

#pragma once

#include <sys/types.h>

#ifdef __cplusplus
# define VQ_API extern "C"
#else
# define VQ_API extern
#endif

typedef void *VQModel;

/*! initially visi-q library; this should be called exactly once,
    before any other call to this library, jsonConfig is a optional
    configuration string that specifies - in json - some configuration
    parametrs (using NULL means 'ignore') */
VQ_API void    vqInit(int *ac, const char ***av, const char *jsonConfig);

/*! tell visi-q library to terminate, and clean up all remaining
    state. this should be called exactly once at the end, and
    (obviously) no calls to this library should be done after that */
VQ_API void    vqTerminate();

/*! create a new model into which we can add geometries, and into
    which new visibility queries can be issues (after vqBuildModel has been called) */
VQ_API VQModel vqNewModel();

/*! add a new triangl emesh to this model, with the given vertex and
    index arrays. This function can be called multiple times, but only
    after a model has been created (\see vqNewModel), and before this
    model gets built (\see vqBuildModel) */
VQ_API void    vqAddTriangleMesh(VQModel model,
                                 const float   *vertexArray, 
                                 size_t         numVertices, 
                                 size_t         sizeOfVertex,
                                 const int32_t *triangleArray, 
                                 size_t         numTriangles, 
                                 size_t         sizeOfTriangle,
                                 /*! if set to true, the back-end can
                                     rely on the data to be
                                     persistent; ie, to not be moved,
                                     deallocated, or modified, and
                                     will thus share the data arrays
                                     rather than replicating them */
                                 bool           sharedPersistentData);

/*! build a model; meaning that its acceleration structure can and
    should now be built. This function should be called exactly once
    for each model, after all its geometries have been added, and
    before any visibility queries may be made */
VQ_API void    vqBuildModel(VQModel model);

/*! destroy a given model, and release all its allocated memory */
VQ_API void    vqDestroyModel(VQModel model);

/*! perform a sequence of visbility test between an array of points A
    and an array of other points B, such that we always test the
    visibility between the i'th point in A and its corresponding i'th
    point in B. Result will be written into isVisible[] array, with 0
    meaninng the two points are not mutually visible, and anything
    else meaning that yes, they are mutually visible. Note this
    function is thread-safe, meaning different threads may perform
    visisbliity queries in parallel */
VQ_API void    vqTestVisibilityBetweenPoints(VQModel model,
                                             const float   *pointsA,
                                             const float   *pointsB,
                                             int32_t *isVisible,
                                             size_t sizeOfPoint,
                                             size_t numPoints);
                                 
/*! trace the given rays (given by origin, direction, and [tMin,tMax]
    array, and write the hit information into geomID (ID of the mesh),
    primID (ID of the triangle inside that meash), and tMax (distance
    to the hit point. Rays that didn't hit anything return geomID=-1 */
VQ_API void    vqTraceRays(size_t numRays,
                           float *origins, 
                           size_t originStride,
                           float *direction, 
                           size_t direcionsStride,
                           float *tMin,
                           size_t tMinStride,
                           float *tMax,
                           size_t tMaxStride,
                           int32_t *geomIDs,
                           size_t geomIDsStride,
                           int32_t *primIDs,
                           size_t primIDsStride
                           );

