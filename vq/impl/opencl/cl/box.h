/* ============================================================================= *\
   MIT License

   Copyright (c) 2018 Ingo Wald

   Permission is hereby granted, free of charge, to any person obtaining a copy
   of this software and associated documentation files (the "Software"), to deal
   in the Software without restriction, including without limitation the rights
   to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
   copies of the Software, and to permit persons to whom the Software is
   furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in all
   copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
   SOFTWARE.
   ============================================================================= */

#include "vec.h"

struct box3f {
  struct vec3f lower, upper;
};

inline struct box3f make_box3f(struct vec3f lower, struct vec3f upper)
{
  struct box3f result;
  result.lower = lower;
  result.upper = upper;
  return result;
}

inline struct box3f make_box3f_empty()
{
  struct box3f result;
  result.lower = make_vec3f_f(const_pos_inf());
  result.upper = make_vec3f_f(const_neg_inf());
  return result;
}

inline struct box3f box3f_make_empty()
{
  struct box3f result;
  result.lower = make_vec3f_f(const_pos_inf());
  result.upper = make_vec3f_f(const_neg_inf());
  return result;
}

inline struct vec3f box3f_size(const struct box3f box)
{
  return vec3f_sub(box.upper,box.lower);
}

inline struct vec3f box3f_center(const struct box3f box)
{
  return vec3f_mul(vec3f_from_f(0.5f),vec3f_add(box.upper,box.lower));
}

inline float box3f_area(const struct box3f box)
{
  struct vec3f diag = box3f_size(box);
  return 2.f*(diag.x+diag.y+diag.z);
}

inline struct box3f box3f_union(struct box3f a, struct box3f b)
{
  return make_box3f(vec3f_min(a.lower,b.lower),
                    vec3f_max(a.upper,b.upper));
}

inline struct box3f box3f_union_vec3f(struct box3f a, struct vec3f b)
{
  return make_box3f(vec3f_min(a.lower,b),
                    vec3f_max(a.upper,b));
}

inline struct box3f vec3f_union_vec3f(struct vec3f a, struct vec3f b)
{
  return make_box3f(vec3f_min(a,b),
                    vec3f_max(a,b));
}

inline void atomic_box3f_union(volatile __global struct box3f *g_box,
                        struct box3f box)
{
  atomic_vec3f_min(&g_box->lower,box.lower);
  atomic_vec3f_max(&g_box->upper,box.upper);
}

