/* ============================================================================= *\
   MIT License

   Copyright (c) 2018 Ingo Wald

   Permission is hereby granted, free of charge, to any person obtaining a copy
   of this software and associated documentation files (the "Software"), to deal
   in the Software without restriction, including without limitation the rights
   to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
   copies of the Software, and to permit persons to whom the Software is
   furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in all
   copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
   SOFTWARE.
   ============================================================================= */

// constant.h
inline float const_pos_inf() { return INFINITY; }
inline float const_neg_inf() { return - INFINITY; }

// atomic.h
inline void atomic_local_minf(volatile __local float *g_val, float myValue)
{
  float cur;
  while (myValue < (cur = *g_val))
    myValue = atomic_xchg(g_val,min(cur,myValue));
}

inline void atomic_minf(volatile __global float *g_val, float myValue)
{
  float cur;
  while (myValue < (cur = *g_val))
    myValue = atomic_xchg(g_val,min(cur,myValue));
}


inline void atomic_local_maxf(volatile __local float *g_val, float myValue)
{
  float cur;
  while (myValue > (cur = *g_val))
    myValue = atomic_xchg(g_val,max(cur,myValue));
}

inline void atomic_maxf(volatile __global float *g_val, float myValue)
{
  float cur;
  while (myValue > (cur = *g_val))
    myValue = atomic_xchg(g_val,max(cur,myValue));
}


