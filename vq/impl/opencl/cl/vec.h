/* ============================================================================= *\
   MIT License

   Copyright (c) 2018 Ingo Wald

   Permission is hereby granted, free of charge, to any person obtaining a copy
   of this software and associated documentation files (the "Software"), to deal
   in the Software without restriction, including without limitation the rights
   to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
   copies of the Software, and to permit persons to whom the Software is
   furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in all
   copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
   SOFTWARE.
   ============================================================================= */

#pragma once

#include "atomic.h"

struct vec3f {
  float x, y, z;
};

struct vec3i {
  int x, y, z;
};

inline struct vec3f make_vec3f(const float x, 
                               const float y, 
                               const float z)
{
  struct vec3f v;
  v.x = x;
  v.y = y;
  v.z = z;
  return v;
}

inline struct vec3f vec3f_sub(const struct vec3f a, const struct vec3f b)
{
  return make_vec3f(a.x-b.x,
                    a.y-b.y,
                    a.z-b.z);
}

inline struct vec3f vec3f_add(const struct vec3f a, const struct vec3f b)
{
  return make_vec3f(a.x+b.x,
                    a.y+b.y,
                    a.z+b.z);
}

inline float vec3f_get(const struct vec3f vec, int dim)
{
  return (&vec.x)[dim];
}

inline struct vec3f vec3f_mul(const struct vec3f a, const struct vec3f b)
{
  return make_vec3f(a.x*b.x,
                    a.y*b.y,
                    a.z*b.z);
}

inline struct vec3f vec3f_rcp(const struct vec3f a)
{
  return make_vec3f(1.f/a.x,
                    1.f/a.y,
                    1.f/a.z);
}

inline struct vec3f vec3f_safe_rcp(const struct vec3f a)
{
  return make_vec3f(1.f/(a.x==0.f?1e-6f:a.x),
                    1.f/(a.y==0.f?1e-6f:a.y),
                    1.f/(a.z==0.f?1e-6f:a.z));
}

inline struct vec3f make_vec3f_f(const float f)
{
  struct vec3f v;
  v.x = v.y = v.z = f;
  return v;
}

inline struct vec3f vec3f_from_f(const float f)
{
  struct vec3f v;
  v.x = v.y = v.z = f;
  return v;
}

inline struct vec3f min3f(struct vec3f a, struct vec3f b)
{
  return make_vec3f(min(a.x,b.x),
                min(a.y,b.y),
                min(a.z,b.z));
}

inline struct vec3f max3f(struct vec3f a, struct vec3f b)
{
  return make_vec3f(max(a.x,b.x),
                max(a.y,b.y),
                max(a.z,b.z));
}

inline struct vec3f vec3f_min(struct vec3f a, struct vec3f b)
{
  return make_vec3f(min(a.x,b.x),
                    min(a.y,b.y),
                    min(a.z,b.z));
}

inline struct vec3f vec3f_max(struct vec3f a, struct vec3f b)
{
  return make_vec3f(max(a.x,b.x),
                    max(a.y,b.y),
                    max(a.z,b.z));
}


inline void atomic_vec3f_min(volatile __global struct vec3f *g_vec, struct vec3f vec)
{
  atomic_minf(&g_vec->x,vec.x);
  atomic_minf(&g_vec->y,vec.y);
  atomic_minf(&g_vec->z,vec.z);
}

inline void atomic_vec3f_max(volatile __global struct vec3f *g_vec, struct vec3f vec)
{
  atomic_maxf(&g_vec->x,vec.x);
  atomic_maxf(&g_vec->y,vec.y);
  atomic_maxf(&g_vec->z,vec.z);
}

