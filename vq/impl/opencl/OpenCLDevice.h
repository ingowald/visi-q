/* ============================================================================= *\
   MIT License

   Copyright (c) 2018 Ingo Wald

   Permission is hereby granted, free of charge, to any person obtaining a copy
   of this software and associated documentation files (the "Software"), to deal
   in the Software without restriction, including without limitation the rights
   to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
   copies of the Software, and to permit persons to whom the Software is
   furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in all
   copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
   SOFTWARE.
   \* ============================================================================= */

#pragma once

// vq
#include "vq/common/Device.h"
// clhelper
#include "clHelper/context.h"
#include "clHelper/buffer.h"
#include "clHelper/embeddedProgram.h"

namespace vq {
  namespace opencl {

    typedef clHelper::Context      CLContext;
    typedef clHelper::Kernel       CLKernel;
    typedef clHelper::Device       CLDevice;
    typedef clHelper::DeviceBuffer CLBuffer;

    struct HostTriangleMesh
    {
      HostTriangleMesh(std::shared_ptr<CLBuffer> vertexBuffer,
                       std::shared_ptr<CLBuffer> triangleBuffer,
                       size_t numTriangles)
        : vertexBuffer(vertexBuffer),
          triangleBuffer(triangleBuffer),
          numTriangles(numTriangles)
      {}

      std::shared_ptr<CLBuffer> vertexBuffer;
      std::shared_ptr<CLBuffer> triangleBuffer;
      size_t numTriangles;
    };

    struct HostModel 
    {
      std::vector<std::shared_ptr<HostTriangleMesh>> triangleMeshes;
      std::shared_ptr<CLBuffer> buildPrimBuffer;
    };

    struct OpenCLDevice : public Device {
      OpenCLDevice();
      virtual ~OpenCLDevice() {};

      /*! implemnetation of vqNewModel */
      virtual ModelHandle newModel() override;

      /*! implementation for vqAddTriangleMesh */
      virtual void addTriangleMesh(ModelHandle modelHandle,
                                   const float   *vertexArray, 
                                   size_t         numVertices, 
                                   size_t         sizeOfVertex,
                                   const int32_t *triangleArray, 
                                   size_t         numTriangles, 
                                   size_t         sizeOfTriangle,
                                   /*! if set to true, the back-end can
                                     rely on the data to be
                                     persistent; ie, to not be moved,
                                     deallocated, or modified, and
                                     will thus share the data arrays
                                     rather than replicating them */
                                   bool           sharedPersistentData)
      override;
      
      /*! implements vqBuildModel */
      virtual void buildModel(ModelHandle modelHandle) override;

      /*! implements vqTestVisibilityBetweenPoints */
      virtual void testVisibilityBetweenPoints(ModelHandle modelHandle,
                                               const float   *pointsA,
                                               const float   *pointsB,
                                               int32_t *isVisible,
                                               size_t sizeOfPoint,
                                               size_t numPoints) override {NOT_IMPLEMENTED; };
      
      /*! implements vqDestroyModel */
      virtual void destroyModel(ModelHandle modelHandle) override { NOT_IMPLEMENTED; };


      std::vector<std::shared_ptr<clHelper::Device>> clDevices;
      std::shared_ptr<clHelper::Device>  clDevice;
      std::shared_ptr<clHelper::Program> clProgram;
      std::shared_ptr<clHelper::Context> clContext;
      std::shared_ptr<clHelper::Kernel>  makeBuildPrimsKernel;
      std::shared_ptr<clHelper::Kernel>  setUpInitialBinning;
      std::shared_ptr<clHelper::Kernel>  singleGroupBinner;
      std::shared_ptr<clHelper::Kernel>  singleGroupPartitioner;
      // std::shared_ptr<clHelper::Kernel>  clearBoundsKernel;
    };


  } // ::vq::ocl
} // ::vq
