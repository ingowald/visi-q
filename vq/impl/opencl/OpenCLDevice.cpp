/* ============================================================================= *\
   MIT License

   Copyright (c) 2018 Ingo Wald

   Permission is hereby granted, free of charge, to any person obtaining a copy
   of this software and associated documentation files (the "Software"), to deal
   in the Software without restriction, including without limitation the rights
   to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
   copies of the Software, and to permit persons to whom the Software is
   furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in all
   copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
   SOFTWARE.
   ============================================================================= */

#include "OpenCLDevice.h"
// ospcommon
#include "ospcommon/tasking/parallel_for.h"
#include "ospcommon/box.h"
#include <memory>

namespace vq {
  namespace opencl {

    using namespace clHelper;
    typedef clHelper::Context CLContext;

    using ospcommon::vec3f;
    using ospcommon::box3f;
    using ospcommon::vec3i;
    using ospcommon::divRoundUp;

    OpenCLDevice::OpenCLDevice()
    {
      std::cout << "querying opencl devices" << std::endl;
      clDevices = clHelper::getAllDevices();
      if (clDevices.empty())
        throw std::runtime_error("no opencl devices found");
      PRINT(clDevices.size());

      std::cout << "selecting opencl device" << std::endl;
      clDevice = clDevices[0];
      PRINT(clDevice->localMemSize);
      PRINT(clDevice->maxComputeUnits);

      std::cout << "creating opencl context" << std::endl;
      clContext = clHelper::Context::create(clDevice);

      std::cout << "building opencl program" << std::endl;
      const std::string programCode = clHelper::getEmbeddedProgram("visiq.cl");
      clProgram
        = clContext->createProgram(programCode);
      makeBuildPrimsKernel
        = std::make_shared<CLKernel>(clProgram,"makeBuildPrims");
      setUpInitialBinning
        = std::make_shared<CLKernel>(clProgram,"setUpInitialBinning");
      PRINT(setUpInitialBinning->getWorkGroupInfoString());
      singleGroupBinner
        = std::make_shared<CLKernel>(clProgram,"singleGroupBinner");
      PRINT(singleGroupBinner->getWorkGroupInfoString());
      singleGroupPartitioner
        = std::make_shared<CLKernel>(clProgram,"singleGroupPartitioner");
      PRINT(singleGroupPartitioner->getWorkGroupInfoString());
      // clearBoundsKernel
      //   = std::make_shared<CLKernel>(clProgram,"clearBounds");
    }


      /*! implemnetation of vqNewModel */
    Device::ModelHandle OpenCLDevice::newModel()  
    {
      HostModel *model = new HostModel;
      return (Device::ModelHandle)model;
    };


    /*! create a (dense) opencl device buffer of given number of
      elements of given 'ElementType', which are stored in a
      (host-side) array 'ptr' with stride 'stride' */
    template<typename ElementType>
    std::shared_ptr<CLBuffer> 
    makeCLBuffer(const std::shared_ptr<CLContext> &context,
                 const void *ptr,
                 size_t numElements,
                 size_t stride=sizeof(ElementType))
    {
      // first, compact everything
      std::vector<ElementType> compacted(numElements);
      for (size_t i=0;i<numElements;i++)
        compacted[i] = *(const ElementType*)((const unsigned char *)ptr + i*stride);

      std::shared_ptr<CLBuffer> buffer 
        = std::make_shared<CLBuffer>(context,numElements*sizeof(ElementType));
      buffer->write(&compacted[0],numElements*sizeof(ElementType));
      return buffer;
    }
                                               

    /*! implementation for vqAddTriangleMesh */
    void OpenCLDevice::addTriangleMesh(ModelHandle modelHandle,
                                       const float   *vertexArray, 
                                       size_t         numVertices, 
                                       size_t         sizeOfVertex,
                                       const int32_t *triangleArray, 
                                       size_t         numTriangles, 
                                       size_t         sizeOfTriangle,
                                       /*! if set to true, the back-end can
                                         rely on the data to be
                                         persistent; ie, to not be moved,
                                         deallocated, or modified, and
                                         will thus share the data arrays
                                         rather than replicating them */
                                       bool           sharedPersistentData)
    {
      HostModel *model = (HostModel *)modelHandle;
      std::shared_ptr<CLBuffer> vertexBuffer
        = makeCLBuffer<vec3f>(clContext,vertexArray,numVertices,sizeOfVertex);
      std::shared_ptr<CLBuffer> triangleBuffer
        = makeCLBuffer<vec3i>(clContext,triangleArray,numTriangles,sizeOfTriangle);
      
      model->triangleMeshes.push_back
        (std::make_shared<HostTriangleMesh>(vertexBuffer,triangleBuffer,numTriangles));

      
    }

    struct CLBuildPrim {
      vec3f lower, upper;
      int geomID, primID;
    };




    struct CLBinJob {
      box3f centBounds;
      int   begin, end;
      int   out_splitDim;
      float out_splitPos;
      int   out_numLeft;
      int   actual_numLeft;

  float out_dbg0;
  float out_dbg1;
#define NUM_BINS 16

  float dbg_Al[3][NUM_BINS-1];
  float dbg_Ar[3][NUM_BINS-1];
  float dbg_cost[3][NUM_BINS-1];
  int   dbg_Nl[3][NUM_BINS-1];
  int   dbg_Nr[3][NUM_BINS-1];
    };

    /*! implements vqBuildModel */
    void OpenCLDevice::buildModel(ModelHandle modelHandle) 
    {
      int numTriangles = 0;
      HostModel *model = (HostModel *)modelHandle;
      for (auto mesh : model->triangleMeshes)
        numTriangles += mesh->numTriangles;
      std::cout << "building model for " << numTriangles << " triangles" << std::endl;

      // ------------------------------------------------------------------
      std::cout << " - creating build prims" << std::endl;
      std::shared_ptr<CLBuffer> buildPrimBuffer
        = CLBuffer::create(clContext,numTriangles*sizeof(CLBuildPrim));
      std::shared_ptr<CLBuffer> partitionedBuildPrimsBuffer
        = CLBuffer::create(clContext,numTriangles*sizeof(CLBuildPrim));
      int meshBegin = 0;
      for (int geomID=0;geomID<model->triangleMeshes.size();geomID++) {
        std::shared_ptr<HostTriangleMesh> 
          mesh = model->triangleMeshes[geomID];
        int thisNumTris = mesh->numTriangles;
        int localSize = 128;
        int globalSize = thisNumTris; //divRoundUp(thisNumTris,localSize);
        makeBuildPrimsKernel->run(Kernel::Args()
                                  .add(buildPrimBuffer)
                                  .add(mesh->vertexBuffer)
                                  .add(mesh->triangleBuffer)
                                  .add(&geomID,sizeof(geomID))
                                  .add(&meshBegin,sizeof(meshBegin))
                                  .add(&thisNumTris,sizeof(thisNumTris)),
                                  globalSize,localSize);
        meshBegin += thisNumTris;
      }

      // ------------------------------------------------------------------
      std::cout << " - computing global bounding box" << std::endl;
      std::shared_ptr<CLBuffer> initialBinJobBuffer;
      {
        CLBinJob initialBinJob;
        initialBinJob.centBounds = box3f();
        initialBinJob.begin = 0;
        initialBinJob.end   = numTriangles;
        initialBinJobBuffer
          = std::make_shared<CLBuffer>(clContext,sizeof(initialBinJob));
        // clearBoundsKernel->run(Kernel::Args().add(initialBinJobBuffer),1,1);
        initialBinJobBuffer->write(&initialBinJob,sizeof(initialBinJob));
        
        // HAS to be 128, the kernel is hardcoded to this  for the reductions
        const int localSize = 128;

        const int numComputeUnits = clDevice->maxComputeUnits;
        const int maxNumWorkGroups = divRoundUp(numTriangles,localSize);
        // compute num items per thread such that each CU gets only
        // (at most) one group
        const int primsPerThread = divRoundUp(maxNumWorkGroups,numComputeUnits);
        const int actualPrimsPerGroup = primsPerThread*localSize;
        const int atualNumGroups = divRoundUp(numTriangles,actualPrimsPerGroup);
        const int globalSize = atualNumGroups * actualPrimsPerGroup;
        PRINT(globalSize);
        PRINT(localSize);
        setUpInitialBinning->run(Kernel::Args()
                                 .add(initialBinJobBuffer)
                                 .add(buildPrimBuffer)
                                 .add(primsPerThread)
                                 .add(numTriangles),
                                 globalSize,localSize);
        initialBinJobBuffer->read(&initialBinJob,sizeof(initialBinJob));
        PRINT(initialBinJob.centBounds);
      }
      // ------------------------------------------------------------------
      std::cout << " - *evaluating* first bin step (single CU for now)" << std::endl;
      {
        
        // HAS to be 128, the kernel is hardcoded to this  for the reductions
        const int localSize = /* NUM_BIN_THREADS */ 32;
        const int globalSize = localSize;

        PRINT(globalSize);
        PRINT(localSize);
        singleGroupBinner->run(Kernel::Args()
                               .add(buildPrimBuffer)
                               .add(initialBinJobBuffer)
                               .add(1),
                               globalSize,localSize);
        {
          CLBinJob initialBinJob;
          initialBinJobBuffer->read(&initialBinJob,sizeof(initialBinJob));
          // PRINT(initialBinJob.centBounds);
          PRINT(initialBinJob.out_splitDim);
          PRINT(initialBinJob.out_splitPos);
          PRINT(initialBinJob.out_numLeft);
          PRINT(initialBinJob.out_dbg0);
          PRINT(initialBinJob.out_dbg1);
          
          for (int dim=0;dim<3;dim++)
            for (int plane=0;plane<NUM_BINS-1;plane++)
              std::cout << "plane " << plane << "/" << dim << ": " 
                        << initialBinJob.dbg_Nl[dim][plane] << " * "
                        << initialBinJob.dbg_Al[dim][plane] << " + "
                        << initialBinJob.dbg_Nr[dim][plane] << " * "
                        << initialBinJob.dbg_Ar[dim][plane] << " = "
                        << initialBinJob.dbg_cost[dim][plane]
                        << std::endl;
          
          PRINT(numTriangles);
        }

        {
          singleGroupPartitioner->run(Kernel::Args()
                                      .add(initialBinJobBuffer)
                                      .add(buildPrimBuffer)
                                      .add(partitionedBuildPrimsBuffer)
                                      .add(1),
                                      globalSize,localSize);
          std::cout << "DONE partitioning ... " << std::endl;
          CLBinJob initialBinJob;
          initialBinJobBuffer->read(&initialBinJob,sizeof(initialBinJob));
          PRINT(initialBinJob.out_numLeft);
          PRINT(initialBinJob.actual_numLeft);
          PRINT(initialBinJob.out_dbg0);
          PRINT(initialBinJob.out_dbg1);
        }
      }


      
      // ------------------------------------------------------------------
      PING;
      std::cout << " .... that's how far I am right now ... exiting" << std::endl;
      exit(1);
    }



    std::shared_ptr<Device> createOpenCLDevice(int *ac, 
                                               const char ***av, 
                                               const char *jsonConfig)
    {
      std::vector<std::shared_ptr<clHelper::Device>> 
        devices = clHelper::getAllDevices();
      return std::make_shared<OpenCLDevice>();
    }

  } // ::vq::ocl
} // ::vq


