/* ============================================================================= *\
   MIT License

   Copyright (c) 2018 Ingo Wald

   Permission is hereby granted, free of charge, to any person obtaining a copy
   of this software and associated documentation files (the "Software"), to deal
   in the Software without restriction, including without limitation the rights
   to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
   copies of the Software, and to permit persons to whom the Software is
   furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in all
   copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
   SOFTWARE.
   ============================================================================= */

#include "cl/box.h"

// ray.h

struct ShadowRay {
  struct vec3f org;
  float t0;
  struct vec3f dir;
  float t;
};

struct TriangleMesh {
  __global struct vec3f *vertex;
  __global struct vec3i *index;
  int numVertices;
  int numTriangles;
};

struct TriAccel {
  struct vec3f v0;
  struct vec3f v1;
  struct vec3f v2;
  int geomID;
  int primID;
};

struct BuildPrim {
  struct box3f bounds;
  int geomID;
  int primID;
};

#define STACK_DEPTH    16
#define RAYS_PER_BLOCK 64

struct BVHNode {
  struct vec3f lower;
  int          numChildren;
  struct vec3f upper;
  int          childID;
};

/*! given a triangle mesh that is to be part of a given scene's
    triangle accels, take all the triangles in this mesh, and write
    them into the appropriate fields in that accel aaray */
__kernel void makeBuildPrims(__global struct BuildPrim *buildPrimsToWrite,
                             __global struct vec3f *vertex,
                             __global struct vec3i *index,
                             int geomID,
                             int firstBuildPrimToWriteForThisMesh,
                             int numTriangles
                             )
{
  const int primID = get_global_id(0);
  if (primID >= numTriangles)
    return;
  __global struct BuildPrim *const buildPrim
    = buildPrimsToWrite + firstBuildPrimToWriteForThisMesh + primID;
  
  const struct vec3f v0 = vertex[index[primID].x];
  const struct vec3f v1 = vertex[index[primID].y];
  const struct vec3f v2 = vertex[index[primID].z];

  buildPrim->bounds = box3f_union_vec3f(vec3f_union_vec3f(v0,v1),v2);

  buildPrim->geomID = geomID;
  buildPrim->primID = primID;
}



// /*! clear the world space bounding box */
// __attribute__ ((reqd_work_group_size(1,1,1)))
// __kernel void clearBounds(/* out: the single world we're computing */
//                           volatile __global struct box3f *worldBounds)
// {
//   *worldBounds = make_box3f_empty();
// }

/*! given an array of build prims, compute the bounding box; note
we'll start this will possibly more threads than there's buidlprims,
because the reductions work easier if we always have multiples of work
group size */
__attribute__ ((reqd_work_group_size(128,1,1)))
__kernel void computeBounds(/* out: the single world we're computing */
                            volatile __global struct box3f *worldBounds,
                            /* in: the array of _all_ build prims */
                            __global struct BuildPrim *buildPrims,
                            // __local  struct box3f *localBounds,
                            int numPrimsPerThread,
                            int numBuildPrims)
{
  __local struct box3f localBounds[128];
  // first, each thread takes a list of prims, and compute a
  // per-thread bbox for that
  const int prim_begin = get_global_id(0)*numPrimsPerThread;
  const int prim_end   = min(prim_begin + numPrimsPerThread, numBuildPrims);
  
  struct box3f bounds = make_box3f_empty();
  for (int prim=prim_begin; prim < prim_end; prim++)
    bounds = box3f_union(bounds,buildPrims[prim].bounds);

  // second, perform per-group reduction (in shared memory, for now)

  // - write to shm:
  const int myID = get_local_id(0);
  localBounds[myID] = bounds;
    barrier(CLK_LOCAL_MEM_FENCE);

  // - now, reduce within the workgroup (todo: try this with reductio
  // - factor of 4 or even 8)
  int reduce_width = get_local_size(0)/2;
  while (reduce_width) {
    if (myID < reduce_width)
      localBounds[myID] = box3f_union(localBounds[myID],
                                      localBounds[myID+reduce_width]);
    reduce_width /= 2;
    barrier(CLK_LOCAL_MEM_FENCE);
  }

  // finally, global reduction
  if (myID == 0)
    atomic_box3f_union(worldBounds,localBounds[0]);
}








#define NUM_BINS 16
#define NUM_BIN_THREADS 32

struct OneBin {
  struct box3f bounds;
  int count;
};

inline void OneBin_clear(__local struct OneBin *bin)
{
  bin->count = 0;
  bin->bounds = box3f_make_empty();
}

struct NBins {
  struct OneBin bin[NUM_BINS];
};

inline void NBins_bin(__local struct NBins *bins, 
                      const float binID_f, 
                      const struct box3f primBounds)
{
  const int binID = min((int)binID_f,NUM_BINS-1);
  bins->bin[binID].bounds
    = box3f_union(bins->bin[binID].bounds,primBounds);
  bins->bin[binID].count ++;
}

struct AllDimBins {
  struct NBins dim[3];
};

inline void AllDimBins_clear(__local struct AllDimBins *myBins)
{
  for (int dim=0;dim<3;dim++)
    for (int bin=0;bin<3;bin++)
      OneBin_clear(&myBins->dim[dim].bin[bin]);
}

inline struct AllDimBins AllDimBins_union(struct AllDimBins a,
                                          struct AllDimBins b)
{
  struct AllDimBins r;
  for (int dim=0;dim<3;dim++)
    for (int bin=0;bin<NUM_BINS;bin++) {
      r.dim[dim].bin[bin].bounds
        = box3f_union(a.dim[dim].bin[bin].bounds,b.dim[dim].bin[bin].bounds);
      r.dim[dim].bin[bin].count
        = a.dim[dim].bin[bin].count + b.dim[dim].bin[bin].count;
    }
  return r;
}


inline void AllDimBins_reduce(__local struct AllDimBins *perThread,
                              int myID, int width)
{
  while ((width/=2) > 0) {
    barrier(CLK_LOCAL_MEM_FENCE);
    if (myID < width) 
      perThread[myID] = AllDimBins_union(perThread[myID],perThread[myID+width]);
  }
  barrier(CLK_LOCAL_MEM_FENCE);
}

/*! scale is pre-multiplied by 2 to save division in 'center' computation */
inline void AllDimBins_binOne(__local struct AllDimBins *myBins,
                              const struct vec3f shift,
                              const struct vec3f scale,
                              const struct BuildPrim buildPrim)
{
  const struct vec3f center
    = box3f_center(buildPrim.bounds);
  const struct vec3f binID_f 
    = vec3f_mul(vec3f_sub(center,shift),scale);
  NBins_bin(&myBins->dim[0],binID_f.x,buildPrim.bounds);
  NBins_bin(&myBins->dim[1],binID_f.y,buildPrim.bounds);
  NBins_bin(&myBins->dim[2],binID_f.z,buildPrim.bounds);
}


struct BinJob {
  struct box3f centBounds;
  int   begin, end;
  int   out_splitDim;
  float out_splitPos;
  int   out_numLeft;
  int   actual_numLeft;
  float out_dbg0;
  float out_dbg1;
  float dbg_Al[3][NUM_BINS-1];
  float dbg_Ar[3][NUM_BINS-1];
  float dbg_cost[3][NUM_BINS-1];
  int   dbg_Nl[3][NUM_BINS-1];
  int   dbg_Nr[3][NUM_BINS-1];
};


/*! given an array of build prims, compute the bounding box; note
we'll start this will possibly more threads than there's buidlprims,
because the reductions work easier if we always have multiples of work
group size */
__attribute__ ((reqd_work_group_size(128,1,1)))
__kernel void setUpInitialBinning(/* out: the single world we're computing */
                                   volatile __global struct BinJob *binJob,
                                   /* in: the array of _all_ build prims */
                                   __global struct BuildPrim *buildPrims,
                                   // __local  struct box3f *localBounds,
                                   int numPrimsPerThread,
                                   int numBuildPrims)
{
  __local struct box3f localBounds[128];
  // first, each thread takes a list of prims, and compute a
  // per-thread bbox for that
  const int prim_begin = get_global_id(0)*numPrimsPerThread;
  const int prim_end   = min(prim_begin + numPrimsPerThread, numBuildPrims);
  
  struct box3f bounds = make_box3f_empty();
  for (int prim=prim_begin; prim < prim_end; prim++)
    bounds = box3f_union_vec3f(bounds,box3f_center(buildPrims[prim].bounds));

  // second, perform per-group reduction (in shared memory, for now)

  // - write to shm:
  const int myID = get_local_id(0);
  localBounds[myID] = bounds;
    barrier(CLK_LOCAL_MEM_FENCE);

  // - now, reduce within the workgroup (todo: try this with reductio
  // - factor of 4 or even 8)
  int reduce_width = get_local_size(0)/2;
  while (reduce_width) {
    if (myID < reduce_width)
      localBounds[myID] = box3f_union(localBounds[myID],
                                      localBounds[myID+reduce_width]);
    reduce_width /= 2;
    barrier(CLK_LOCAL_MEM_FENCE);
  }

  // finally, global reduction
  if (myID == 0)
    atomic_box3f_union(&binJob->centBounds,localBounds[0]);
}







struct SAHSplitPlane {
  struct box3f lBounds, rBounds;
  int lCount, rCount;
};

struct SAHSplitPlanes
{
  struct SAHSplitPlane plane[NUM_BINS-1];
};

struct SAHSplitInfo 
{
  struct SAHSplitPlanes dim[3];
};

inline void SplitInfo_build(__local struct SAHSplitInfo *splitInfo,
                            int dim,
                            __local struct AllDimBins *binInfo)
{
  {
    struct box3f lBounds = box3f_make_empty();
    int lCount = 0;
    for (int plane=0;plane<NUM_BINS-1;plane++) {
      lBounds = box3f_union(lBounds,binInfo->dim[dim].bin[plane].bounds);
      lCount += binInfo->dim[dim].bin[plane].count;
      splitInfo->dim[dim].plane[plane].lBounds = lBounds;
      splitInfo->dim[dim].plane[plane].lCount  = lCount;
    }
  }
  {
    struct box3f rBounds = box3f_make_empty();
    int rCount = 0;
    for (int plane=NUM_BINS-2;plane>=0;plane--) {
      rBounds = box3f_union(rBounds,binInfo->dim[dim].bin[plane+1].bounds);
      rCount += binInfo->dim[dim].bin[plane+1].count;
      splitInfo->dim[dim].plane[plane].rBounds = rBounds;
      splitInfo->dim[dim].plane[plane].rCount  = rCount;
    }
  }
}

/*! a build job where a single workgroup works on a binning job, all
  by itself, with each group doing a different binning job */
__attribute__ ((reqd_work_group_size(NUM_BIN_THREADS,1,1)))
__kernel void singleGroupBinner(__global struct BuildPrim *buildPrim,
                                __global struct BinJob *binJobArray,
                                int numBinJobs
                                )
{
  // ------------------------------------------------------------------
  // the job this group is working on
  // ------------------------------------------------------------------
  const int groupID = get_global_id(0)/NUM_BIN_THREADS;
  __global struct BinJob *ourBinJob = binJobArray + groupID;

  const struct vec3f bin_shift = ourBinJob->centBounds.lower;
  const struct vec3f bin_scale
    = vec3f_mul(vec3f_from_f(NUM_BINS),
                vec3f_safe_rcp(vec3f_sub(ourBinJob->centBounds.upper,
                                         ourBinJob->centBounds.lower)));

  ourBinJob->out_dbg0 = bin_shift.x;
  ourBinJob->out_dbg1 = bin_scale.x;
  // ------------------------------------------------------------------
  // allocate a set of per-thread bins in local memory, and clear them
  // ------------------------------------------------------------------

  // alloc one full binning set per thread...
  __local struct AllDimBins perThreadBin[NUM_BIN_THREADS];
  __local struct SAHSplitInfo splitInfo;
  __local struct AllDimBins *myBins = &perThreadBin[get_local_id(0)];

  // ... and clear it.
  AllDimBins_clear(myBins);
  
  // ------------------------------------------------------------------
  // now, process this group's primitives (bin one per thread...)
  // ------------------------------------------------------------------
  const int ourBegin = ourBinJob->begin;
  const int ourEnd   = ourBinJob->end;
  for (int myPrim=ourBegin + get_local_id(0);myPrim<ourEnd;myPrim+=get_local_size(0))
    AllDimBins_binOne(myBins,bin_shift,bin_scale,buildPrim[myPrim]);
  // ... and reduce
  AllDimBins_reduce(perThreadBin,get_local_id(0),NUM_BIN_THREADS);

  // ------------------------------------------------------------------
  // now. have first N_BINS-1 threads compute the left/right split
  // infos
  // ------------------------------------------------------------------
  // note: already barrier'ed from reduce()
  if (get_local_id(0) < 3) {
    SplitInfo_build(&splitInfo,get_local_id(0),perThreadBin);
  }
  barrier(CLK_LOCAL_MEM_FENCE);
  // if (get_local_id(0) == 1) {
  //   ourBinJob->out_dbg0 = ourBegin;
  //   ourBinJob->out_dbg1 = ourEnd;
  // }

  // ------------------------------------------------------------------
  // one thread per plane computes that plane's cost (in all three
  // dimensions)
  // ------------------------------------------------------------------
  __local float local_bestCost[NUM_BINS-1];
  __local int   local_bestDim[NUM_BINS-1];
  __local int   local_bestLeft[NUM_BINS-1];
  __local float local_dbg0[NUM_BINS-1];
  __local float local_dbg1[NUM_BINS-1];

  if (get_local_id(0) < NUM_BINS-1) {
    const int plane = get_local_id(0);
    const float rcp_area
      = 1.f/box3f_area(box3f_union(splitInfo.dim[0].plane[0].lBounds,
                                   splitInfo.dim[0].plane[0].rBounds));
    int bestDim = -1;
    int bestLeft = -1;
    float bestCost
      = splitInfo.dim[0].plane[0].lCount+splitInfo.dim[0].plane[0].rCount;
    for (int dim=0;dim<3;dim++) {
      const float lArea = box3f_area(splitInfo.dim[dim].plane[plane].lBounds);
      const float rArea = box3f_area(splitInfo.dim[dim].plane[plane].rBounds);
      const int Nl = splitInfo.dim[dim].plane[plane].lCount;
      const int Nr = splitInfo.dim[dim].plane[plane].rCount;


      const float cost = 1.f+rcp_area*(Nl*lArea+Nr*rArea);

      ourBinJob->dbg_Nl[dim][get_local_id(0)] = Nl;
      ourBinJob->dbg_Nr[dim][get_local_id(0)] = Nr;
      ourBinJob->dbg_Al[dim][get_local_id(0)] = lArea;
      ourBinJob->dbg_Ar[dim][get_local_id(0)] = rArea;
      ourBinJob->dbg_cost[dim][get_local_id(0)] = cost;
      
      if (Nl > 0 && Nr > 0 && cost < bestCost) {
        bestCost = cost;
        bestDim  = dim;
        bestLeft = Nl;
      }
    }
    local_bestCost[get_local_id(0)] = bestCost;
    local_bestDim[get_local_id(0)]  = bestDim;
    local_bestLeft[get_local_id(0)] = bestLeft;
  }

  // ourBinJob->out_dbg0 = splitInfo.dim[0].plane[1].lCount;
  // ourBinJob->out_dbg1 = splitInfo.dim[0].plane[1].rCount;

  barrier(CLK_LOCAL_MEM_FENCE);

  //------------------------------------------------------------------
  // last step - first thread reduces across planes
  //------------------------------------------------------------------
  if (get_local_id(0) == 0) {
    int   bestPlane = 0;
    float bestCost  = local_bestCost[0];
    for (int plane=1;plane<NUM_BINS-1;plane++) 
      if (local_bestCost[plane] < bestCost) {
        bestPlane = plane;
        bestCost  = local_bestCost[plane];
      }

    int bestDim = local_bestDim[bestPlane];
    ourBinJob->out_splitDim = bestDim;
    ourBinJob->out_numLeft  = bestDim <  0 ? -1 :local_bestLeft[bestPlane];
    // ourBinJob->out_dbg0 = bestDim <  0 ? -1 : local_dbg0[bestPlane];
    // ourBinJob->out_dbg1 = bestDim <  0 ? -1 :local_dbg1[bestPlane];

    if (bestDim >= 0) {
      const struct box3f allBounds
        = box3f_union(splitInfo.dim[0].plane[0].lBounds,splitInfo.dim[0].plane[0].rBounds);
      ourBinJob->out_splitPos
        = vec3f_get(allBounds.lower,bestDim)
        + ((bestPlane+1.f)/NUM_BINS)*(vec3f_get(allBounds.upper,bestDim)-
                                      vec3f_get(allBounds.lower,bestDim));
    }
  }
}
                       

/*! a build job where a single workgroup works on a binning job, all
  by itself, with each group doing a different binning job */
__attribute__ ((reqd_work_group_size(NUM_BIN_THREADS,1,1)))
__kernel void singleGroupPartitioner(__global struct BinJob *binJobArray,
                                     __global struct BuildPrim *buildPrim_in,
                                     __global struct BuildPrim *buildPrim_out,
                                     int numBinJobs
                                     )
{
  const int ourBinJobID = (get_global_id(0)/get_local_size(0));
  __global struct BinJob *ourBinJob = binJobArray+ourBinJobID;
  const int our_in_begin = ourBinJob->begin;
  const int our_in_end   = ourBinJob->end;
  __local int our_out_l;
  __local int our_out_r;
  our_out_l = ourBinJob->begin;
  our_out_r = ourBinJob->end;
  barrier(CLK_LOCAL_MEM_FENCE);

  const int split_dim = ourBinJob->out_splitDim;
  const float split_pos = ourBinJob->out_splitPos;

  struct BuildPrim my_bp;
  for (int our_in=our_in_begin;our_in<our_in_end;our_in+=get_local_size(0)) {
    const int my_in = our_in+get_local_id(0);

    my_bp = buildPrim_in[my_in];
    int do_write_l = 0;
    int do_write_r = 0;
    if (my_in < our_in_end) {
      if (vec3f_get(box3f_center(my_bp.bounds),split_dim) < split_pos) 
        do_write_l = 1;
      else
        do_write_r = 1;
    }
    {
      int my_out_l = our_out_l + work_group_scan_exclusive_add(do_write_l);
      if (do_write_l)
        buildPrim_out[my_out_l] = my_bp;
      int sum_l = work_group_reduce_add(do_write_l);
      if (get_local_id(0) == 0) 
        our_out_l += sum_l;
      ourBinJob->out_dbg0 = sum_l;
    }
    {
      int sum_r = work_group_reduce_add(do_write_r);
      int my_out_r = our_out_r - sum_r + work_group_scan_exclusive_add(do_write_r);
      if (do_write_r)
        buildPrim_out[my_out_r] = my_bp;
      if (get_local_id(0) == 0) 
        our_out_r -= sum_r;
      ourBinJob->out_dbg1 = sum_r;
    }
    barrier(CLK_LOCAL_MEM_FENCE);
  }
  ourBinJob->actual_numLeft = our_out_r - our_in_begin;;
}
                             
__kernel void traverse(__global struct BVHNode   *bvhNode, 
                       __global struct TriAccel  *triangleAccel,
                       int numTriangles,
                       __global struct ShadowRay *shadowRay,
                       int numRays)
{
  __local int traversalStack[STACK_DEPTH*RAYS_PER_BLOCK];
}
