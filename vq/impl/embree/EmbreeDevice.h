/* ============================================================================= *\
   MIT License

   Copyright (c) 2018 Ingo Wald

   Permission is hereby granted, free of charge, to any person obtaining a copy
   of this software and associated documentation files (the "Software"), to deal
   in the Software without restriction, including without limitation the rights
   to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
   copies of the Software, and to permit persons to whom the Software is
   furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in all
   copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
   SOFTWARE.
   \* ============================================================================= */

#pragma once 

// our stuff
#include "ospcommon/tasking/parallel_for.h"
#include "vq/common/Device.h"
#include "vq/impl/embree/EmbreeModel.h"
// embree
#include "embree3/rtcore.h"

namespace vq {
  namespace embree { 

    struct EmbreeDevice : public Device {
      EmbreeDevice();
      virtual ~EmbreeDevice();

      tbb::task_scheduler_init tbb_init;

      /*! implemnetation of vqNewModel */
      virtual ModelHandle newModel() override;

      /*! implementation for vqAddTriangleMesh */
      virtual void addTriangleMesh(ModelHandle modelHandle,
                                   const float   *vertexArray, 
                                   size_t         numVertices, 
                                   size_t         sizeOfVertex,
                                   const int32_t *triangleArray, 
                                   size_t         numTriangles, 
                                   size_t         sizeOfTriangle,
                                   /*! if set to true, the back-end can
                                     rely on the data to be
                                     persistent; ie, to not be moved,
                                     deallocated, or modified, and
                                     will thus share the data arrays
                                     rather than replicating them */
                                   bool           sharedPersistentData)
      override;
      
      /*! implements vqBuildModel */
      virtual void buildModel(ModelHandle modelHandle) override;

      /*! implements vqTestVisibilityBetweenPoints */
      virtual void testVisibilityBetweenPoints(ModelHandle modelHandle,
                                               const float   *pointsA,
                                               const float   *pointsB,
                                               int32_t *isVisible,
                                               size_t sizeOfPoint,
                                               size_t numPoints) override;
      
      /*! implements vqDestroyModel */
      virtual void destroyModel(ModelHandle modelHandle) override;

      std::mutex mutex;

      RTCDevice device { nullptr };
    };

  } // ::vq::embree
} // ::vq
