/* ============================================================================= *\
MIT License

Copyright (c) 2018 Ingo Wald

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
\* ============================================================================= */

#include "vq/impl/embree/EmbreeModel.h"
#include "vq/impl/embree/EmbreeDevice.h"

namespace vq {
  namespace embree { 

    EmbreeModel::EmbreeModel(std::shared_ptr<EmbreeDevice> device)
      : device(device)
    {
      scene = rtcNewScene(device->device);
    }
  
    EmbreeModel::~EmbreeModel()
    {
      rtcReleaseScene(scene);
    }

    void EmbreeModel::addTriangleMesh(const float   *vertexArray, 
                                      size_t         numVertices, 
                                      size_t         sizeOfVertex,
                                      const int32_t *triangleArray, 
                                      size_t         numTriangles, 
                                      size_t         sizeOfTriangle,
                                      /*! if set to true, the back-end can
                                        rely on the data to be
                                        persistent; ie, to not be moved,
                                        deallocated, or modified, and
                                        will thus share the data arrays
                                        rather than replicating them */
                                      bool           sharedPersistentData)
    {
      RTCGeometry geom = rtcNewGeometry(device->device,RTC_GEOMETRY_TYPE_TRIANGLE);
      rtcEnableGeometry(geom);
      
      if (sharedPersistentData) {
        rtcSetSharedGeometryBuffer(geom,RTC_BUFFER_TYPE_INDEX,0,RTC_FORMAT_UINT3,
                                   
                                   triangleArray,0,sizeOfTriangle,numTriangles);
        rtcSetSharedGeometryBuffer(geom,RTC_BUFFER_TYPE_VERTEX,0,RTC_FORMAT_FLOAT3,
                                   vertexArray,0,sizeOfVertex,numVertices);
      } else {

        RTCBuffer vertexBuffer, triangleBuffer;
        {
          std::lock_guard<std::mutex> lock(device->mutex);
          vertexBuffer = rtcNewBuffer(device->device,numVertices*sizeOfVertex);
          triangleBuffer = rtcNewBuffer(device->device,numTriangles*sizeOfTriangle);
        }
        
        rtcSetGeometryBuffer(geom,RTC_BUFFER_TYPE_INDEX,0,RTC_FORMAT_UINT3,
                             triangleBuffer,0,sizeOfTriangle,numTriangles);
        int *triangleBufferData
          = (int *)rtcGetGeometryBufferData(geom,RTC_BUFFER_TYPE_INDEX,0);
        memcpy(triangleBufferData,triangleArray,numTriangles*sizeOfTriangle);
        rtcUpdateGeometryBuffer(geom,RTC_BUFFER_TYPE_INDEX,0);

        rtcSetGeometryBuffer(geom,RTC_BUFFER_TYPE_VERTEX,0,RTC_FORMAT_FLOAT3,
                             vertexBuffer,0,sizeOfVertex,numVertices);
        float *vertexBufferData
          = (float *)rtcGetGeometryBufferData(geom,RTC_BUFFER_TYPE_VERTEX,0);
        memcpy(vertexBufferData,vertexArray,numVertices*sizeOfVertex);
        rtcUpdateGeometryBuffer(geom,RTC_BUFFER_TYPE_VERTEX,0);
      }
      
      rtcCommitGeometry(geom);
      rtcAttachGeometry(scene,geom);
      rtcReleaseGeometry(geom);
    }

    /*! implements vqBuildModel */
    void EmbreeModel::build()
    {
      /* Commits the scene. */
      rtcCommitScene(scene);
      // RTCBounds bounds;
      // rtcGetSceneBounds(scene, &bounds);
      // PRINT((const ospcommon::vec3f&)bounds);
    }


  } // ::vq::embree
} // ::vq
