/* ============================================================================= *\
MIT License

Copyright (c) 2018 Ingo Wald

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
\* ============================================================================= */

#pragma once

// embree
#include "embree3/rtcore.h"
#include <memory>

namespace vq {
  namespace embree { 
    
    struct EmbreeDevice;

    struct EmbreeModel {
      EmbreeModel(std::shared_ptr<EmbreeDevice> device);
      ~EmbreeModel();

      /*! implements vqBuildModel */
      void build();

      void addTriangleMesh(const float   *vertexArray, 
                           size_t         numVertices, 
                           size_t         sizeOfVertex,
                           const int32_t *triangleArray, 
                           size_t         numTriangles, 
                           size_t         sizeOfTriangle,
                           /*! if set to true, the back-end can
                             rely on the data to be
                             persistent; ie, to not be moved,
                             deallocated, or modified, and
                             will thus share the data arrays
                             rather than replicating them */
                           bool           sharedPersistentData);
      
    // private:
      RTCScene      scene;
      std::shared_ptr<EmbreeDevice> device;
    };

  } // ::vq::embree
} // ::vq
