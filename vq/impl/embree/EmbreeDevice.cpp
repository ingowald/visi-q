/* ============================================================================= *\
   MIT License

   Copyright (c) 2018 Ingo Wald

   Permission is hereby granted, free of charge, to any person obtaining a copy
   of this software and associated documentation files (the "Software"), to deal
   in the Software without restriction, including without limitation the rights
   to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
   copies of the Software, and to permit persons to whom the Software is
   furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in all
   copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
   SOFTWARE.
   \* ============================================================================= */

// our stuff
#include "EmbreeDevice.h"
// ospcommon
#include "ospcommon/tasking/parallel_for.h"

namespace vq {
  namespace embree {
    
    EmbreeDevice::EmbreeDevice()
    {
      const char *embreeConfig = NULL;
      if (getenv("VISIQ_DEBUG") && atoi(getenv("VISIQ_DEBUG")))
        embreeConfig = "threads=1,verbose=2";
      device = rtcNewDevice(NULL);
    }

    EmbreeDevice::~EmbreeDevice()
    {
      rtcReleaseDevice(device);
    }
  
    /*! implemnetation of vqNewModel */
    Device::ModelHandle EmbreeDevice::newModel() 
    {
      std::lock_guard<std::mutex> lock(this->mutex);
      EmbreeModel *model = new EmbreeModel(std::dynamic_pointer_cast<EmbreeDevice>(shared_from_this()));
      return (Device::ModelHandle)model;
    }

      /*! implementation for vqAddTriangleMesh */
    void EmbreeDevice::addTriangleMesh(ModelHandle modelHandle,
                                       const float   *vertexArray, 
                                       size_t         numVertices, 
                                       size_t         sizeOfVertex,
                                       const int32_t *triangleArray, 
                                       size_t         numTriangles, 
                                       size_t         sizeOfTriangle,
                                       /*! if set to true, the back-end can
                                         rely on the data to be
                                         persistent; ie, to not be moved,
                                         deallocated, or modified, and
                                         will thus share the data arrays
                                         rather than replicating them */
                                       bool           sharedPersistentData)
    {
      assert(modelHandle);
      EmbreeModel *model = (EmbreeModel *)modelHandle;
      model->addTriangleMesh(vertexArray,numVertices,sizeOfVertex,
                             triangleArray,numTriangles,sizeOfTriangle,
                             sharedPersistentData);
    }

      
      /*! implements vqBuildModel */
    void EmbreeDevice::buildModel(ModelHandle modelHandle) 
    {
      assert(modelHandle);
      EmbreeModel *model = (EmbreeModel *)modelHandle;
      model->build();
    }

      /*! implements vqTestVisibilityBetweenPoints */
    void EmbreeDevice::testVisibilityBetweenPoints(ModelHandle modelHandle,
                                                   const float   *pointsA,
                                                   const float   *pointsB,
                                                   int32_t *isVisible,
                                                   size_t sizeOfPoint,
                                                   size_t numPoints) 
    {
      assert(modelHandle);
      EmbreeModel *model = (EmbreeModel *)modelHandle;

      const int batchSize = 128;
      tasking::parallel_in_blocks_of<batchSize>
        (numPoints,[&](const size_t begin, const size_t end){
          RTCRayNt<batchSize> rays;
          for (size_t i=begin;i<end;i++) {
            const size_t rID = i-begin;
            rays.org_x[rID] = pointsA[(sizeOfPoint/sizeof(float))*i+0];
            rays.org_y[rID] = pointsA[(sizeOfPoint/sizeof(float))*i+1];
            rays.org_z[rID] = pointsA[(sizeOfPoint/sizeof(float))*i+2];

            rays.dir_x[rID] = pointsB[(sizeOfPoint/sizeof(float))*i+0]
              - rays.org_x[rID];
            rays.dir_y[rID] = pointsB[(sizeOfPoint/sizeof(float))*i+1]
              - rays.org_y[rID];
            rays.dir_z[rID] = pointsB[(sizeOfPoint/sizeof(float))*i+2]
              - rays.org_z[rID];
            // PRINT(rays.org_x[rID]);
            // PRINT(rays.org_y[rID]);
            // PRINT(rays.org_z[rID]);
            // PRINT(rays.dir_x[rID]);
            // PRINT(rays.dir_y[rID]);
            // PRINT(rays.dir_z[rID]);

            rays.time[rID]  = 0.f;
            rays.mask[rID]  = 0;
            rays.id[rID]    = rID;
            rays.flags[rID] = 0;
            rays.tnear[rID] = 1e-6f;
            rays.tfar[rID]  = 1.f - 1e-6f;
          }

          RTCIntersectContext context;
          rtcInitIntersectContext(&context);
          rtcOccludedNM(model->scene,&context,(RTCRayN*)&rays,batchSize,1,sizeof(rays));

          for (size_t i=begin;i<end;i++) {
            const size_t rID = i-begin;
            // PRINT(rays.tfar[rID]);
            isVisible[i] = rays.tfar[rID] >= 0.f;
          }
        });
    }

      /*! implements vqDestroyModel */
    void EmbreeDevice::destroyModel(ModelHandle modelHandle) 
    {
      assert(modelHandle);
      EmbreeModel *model = (EmbreeModel *)modelHandle;
      delete model;
    }


    /*! our 'hook' into the visiq-embree library - all we have to see is
      that we can create a device, so we don't actually have to
      include any header files from that backend at all */
    std::shared_ptr<Device> createEmbreeDevice(int *ac, 
                                               const char ***av, 
                                               const char *jsonConfig)
    {
      return std::make_shared<EmbreeDevice>();
    }

  } // ::vq::embree
} // ::vq
